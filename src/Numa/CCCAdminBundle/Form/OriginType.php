<?php

namespace Numa\CCCAdminBundle\Form;

use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class OriginType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $options['container'];
        $csr = $container->get('security.authorization_checker')->isGranted("ROLE_OCR");
        $types = "v.type=1";
        if ($csr) {
            $types = "v.type=1 or v.type=2";
        }
        $builder
            ->add('po', 'text', array('label' => 'PO# or Reference#', 'required' => false))
            ->add('refno', 'text', array('attr' => array('class' => 'tagsinput', 'data-role' => 'tagsinput', 'maxlength' => 50),'label' => '(LTD only) Reference#', 'required' => false))
            ->add('collect', null, array('label' => 'Collect', 'required' => false))
            ->add('building_business', 'text', array('attr' => array('maxlength' => 26), 'label' => 'Pickup Building / Business Name * ', 'required' => true))
            ->add('address', 'text', array('attr' => array('maxlength' => 26), 'label' => 'Pickup Address', 'required' => false))
            ->add('contact_person', 'text', array('attr' => array('maxlength' => 22), 'label' => 'Pickup Contact Person', 'required' => false))
            ->add('time_flag', null, array('label' => 'Change Time', 'required' => false))
            ->add('delivery_time', 'datetime', array('label' => 'Requested Pickup Time', 'required' => false, 'data' => new \DateTime()))
            //->add('VehicleType', null, array('label' => 'Vehicle Type * ', 'required' => true))
            ->add('VehicleType', 'entity', array(
                'class' => 'NumaCCCAdminBundle:Vehtypes',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $er) use ($types) {
                    return $er->createQueryBuilder('v')
                        ->where($types)
                        ->addOrderBy('v.vehcode', 'ASC');
                },
                'choice_attr' => function (Vehtypes $vehtypes, $key, $index) {
                    $class = "color:green !important";
                    if($vehtypes->getType()==2){
                        $class = "color:red !important";
                    }
                    return ['style' => $class ];
                }
            ))
            ->add('pieces', 'number', array('attr' => array('maxlength' => 6, 'type' => "number"), 'label' => 'Pieces * ', 'required' => true))
            ->add('weight', 'number', array('attr' => array('maxlength' => 6, 'type' => "number"), 'label' => 'Weight', 'required' => false))
            ->add('cod_amount', 'text', array('label' => 'COD amount', 'required' => false))
            ->add('comments', null, array('attr' => array('maxlength' => 130), 'label' => 'Comments and/or Commodity Instructions', 'required' => false))
            ->add('dimensions', null, array('attr' => array('maxlength' => 100), 'label' => '(LTD only) Dimensions', 'required' => false))
            ->add('miscinfo', null, array('attr' => array('maxlength' => 400), 'label' => '(LTD only) Misc. Info.', 'required' => false))
            ->add('serv_type', 'choice', array('attr' => array('maxlength' => 3), 'label' => 'Serv Type *', 'required' => true, 'choices' => array('REG' => 'REG', 'DIR' => "DIR")));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Origin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'numa_cccadminbundle_origin';
    }

    public function getParent()
    {
        return 'container_aware';
    }
}
