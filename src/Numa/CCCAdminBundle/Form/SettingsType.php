<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public $name;
    public function __construct($name="") {
        $this->name=$name;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('name','hidden')
            ->add('value',null,array('label' => $this->name))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Settings'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        //dump("getName".$this->name);
        if(empty($this->name)){
            return 'numa_cccadminbundle_settings';
        }
        return $this->name;
    }
    
    public function setName($name){
        //dump("setName".$name);
        $this->name = $name;
    }
}
