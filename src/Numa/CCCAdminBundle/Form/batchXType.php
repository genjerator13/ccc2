<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class batchXType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('started')
            ->add('closed')
            ->add('working_days','choice',array('attr'=>array('class'=>"size50"),'label'=>"Working Days",'choices'=>array(range(0, 31))))
            ->add('file','file', array(
                'data_class' => null, 'required'=>false, 'label'=>'S3DB Probills File'
            ))
            ->add('newsletter','file', array(
                'data_class' => null, 'required'=>false, 'label'=>'Newsletter PDF'
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\batchX'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'numa_cccadminbundle_batchx';
    }
}
