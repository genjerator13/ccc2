<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Numa\CCCAdminBundle\Events\DispatchSubscriber;

class DispatchcardType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('serv_type', 'choice', array('attr'=>array('maxlength'=>3), 'label' => 'Serv Type *', 'required' => true, 'choices' => array('REG' => 'REG', 'DIR' => "DIR")))
                ->add('call_in_buy', 'text', array('attr'=>array('maxlength'=>15), 'label' => 'Call in by *', 'required' => true))
                //->add('po', 'text', array('attr'=>array('maxlength'=>15),'label' => 'PO#', 'required' => false))
                ->add('origin', 'collection', array('options' => array('label' => false), 'type' => new OriginType(), 'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true))
                ->add('destination', 'collection', array('options' => array('label' => false), 'type' => new DestinationType(), 'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true))
        ;

        $builder->addEventSubscriber(new DispatchSubscriber($options['container']));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Dispatchcard'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'numa_cccadminbundle_dispatchcard';
    }

    public function getParent()
    {
        return 'container_aware';
    }

}
